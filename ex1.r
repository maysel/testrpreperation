#Exercise 1 

#Use dataset airquality (part of the R bundle) 

#Calculate the number of rows with NA�s
#Remove all the rows with NA�s
#Produce a vector with all average measures (Ozone Solar.R Wind Temp)
#Filter the dataset for rows that are above average in temperature 
#Sort the dataset according to solar radiation 
#Add a column to the dataset that shows a ratio between Ozone and Solar.R (use the apply function properly) 
#Plot on the same page all 6 graphs of one attribute against another (e.g. Ozone against Solar.R). From the graphs which variables seem related? 

#Calculate the number of NA's
sum(rowSums(is.na(airquality))) #sum not count

#Remove all the rows with NA's
row.has.na <- apply(airquality, 1, function(x){any(is.na(x))}) #get all values with na
airqualityWithoutNa <- airquality[!row.has.na,] #remove all the na values
any(is.na(airqualityWithoutNa)) #test there no na values
nrow(airqualityWithoutNa) #test

#Calculate the number of rows with NA's
nrow(airquality)-nrow(airqualityWithoutNa)

#Produce a vector with all average measures (Ozone Solar.R Wind Temp)
#1-rows, 2-column
meanVec <- apply(airqualityWithoutNa, 2, mean)

#Filter the dataset for rows that are above average in temperature
airqualityWithoutNa[airqualityWithoutNa$Temp>mean(airqualityWithoutNa$Temp),]

#Sort the dataset according to solar radiation 
sortBySolar <- airqualityWithoutNa[order(airqualityWithoutNa$Solar.R),]

#Add a column to the dataset that shows a ratio between Ozone and Solar.R (use the apply function properly)
airqualityWithoutNa$ratio <- airqualityWithoutNa$Ozone/airqualityWithoutNa$Solar.R

#Plot on the same page all 6 graphs of one attribute against another (e.g. Ozone against Solar.R). 
#From the graphs which variables seem related?
par(mfrow = c(6,1))
plot(airqualityWithoutNa$Ozone,airqualityWithoutNa$Solar.R)
plot(airqualityWithoutNa$Ozone,airqualityWithoutNa$Wind)
plot(airqualityWithoutNa$Ozone,airqualityWithoutNa$Temp)
plot(airqualityWithoutNa$Ozone,airqualityWithoutNa$Month)
plot(airqualityWithoutNa$Ozone,airqualityWithoutNa$Day)
plot(airqualityWithoutNa$Ozone,airqualityWithoutNa$ratio)

#END OF EX1
