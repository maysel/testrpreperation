#Ex8 - Decision trees.r

#For df
install.packages('ISLR')
library(ISLR)

df <- College
str(df)

#Plot Room.Board against Grad.Rate while color is Private what is the conclusion?
library(ggplot2)
ggplot(df, aes(x=df$Room.Board, y=df$Grad.Rate))+geom_point(alpha=0.2)
#Conclusion - we see a weack affect between them

#Plot a histogram of Grad.Rate and show how Private is distributed in each bar what is the conclusion? 
ggplot(df, aes(x=df$Grad.Rate, fill = df$Private)) + geom_histogram()
#Conclusion - there is no relation between them

#Plot a histogram of F.Undergrad and show how Private is distributed in each bar what is the conclusion?  
ggplot(df, aes(x=df$F.Undergrad, fill = df$Private)) + geom_histogram()
#Conclusion - there is bigger distribution for college that not private

#Fix the problem of schools with Grad.Rate above 100%
#find who is above rate of 100%
filter <- df[,"Grad.Rate"] > 100
df[filter,]
#Cazenovia College

#We defined them 100% 
df['Cazenovia College', 'Grad.Rate'] <- 100

#Divide the data into train set and test set 
library(caTools) #for the split
split <- sample.split(df, 0.7)
train <- subset(df, split==T)
test <- subset(df, split==F)

install.packages('rpart')
library(rpart) #for kyphosis and for Decision trees

#Derive the tree model 
tree <- rpart(Private ~ .,method = 'class' ,data = train)

#Plot the model 
library(rpart.plot) #for tree plot
prp(tree)

predicted <- predict(tree,test)

predicted <- as.data.frame(predicted) #we recived probability

#above 0.5= yes
joiner <- function(x){
  if(x > 0.5){
    return('Yes')
  }else{
    return('No')
  }
}

head(predicted)

predicted$private <- sapply(predicted$Yes, joiner)

#Compute confusion matrix, Recall and Precision 
confusion <- table(predicted$private, test$Private)

#     No Yes
#No   64  12
#Yes   9 174

class(confusion)

TN <- confusion[1,1] #64
FN <- confusion[1,2] #12
FP <- confusion[2,1] #9
TP <- confusion[2,2] #174

recall <- TP/(TP+FN) #0.935
precision <- TP/(TP+FP) #0.951


#END OF EX8