#EX9 - Decision Tree, Random Forest, Naive Bayes

#Decision trees 

install.packages('rpart')
library(rpart) #for kyphosis and for Decision trees

#Kyphosis
#a factor with levels absent present indicating if a kyphosis (a type of deformation) was present after the operation.

#Age
#in months

#Number
#the number of vertebrae involved

#Start
#the number of the first (topmost) vertebra operated on.


str(kyphosis)
tree <- rpart(Kyphosis ~ ., method = 'class', data = kyphosis)

install.packages('rpart.plot')
library(rpart.plot) #for tree plot

prp(tree)

predicted <- predict(tree,kyphosis)

predicted <- as.data.frame(predicted)

round(predicted$present,digits=2)

#END OF EX9