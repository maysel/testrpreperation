#Ex5 - Logistic Regression

#The task in this exercise is to build a model to predict the income of citizens according to a census.
#The prediction should be whether the income is above or below 50K dollars.

#Read the file use: adult <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
#Reduce the number of levels for country, use - 
  
#Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
#              "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
#              "Portugal")


#Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
#          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam"    )


#North.america <- c("United-States" ,  "Canada", "Puerto-Rico"  ) 

#Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
#                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
#                            "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
#                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
#Other <- c("South")

#And also use the operator %in% in the following manner: 
  #if(ctry %in%  Europe)
    
#    Reduce the number of levels for ‘marital’ (3 levels) and type_employer (5 levels)
#Remove all records with missing data 
#Remove the index column from the dataset 
#Use histograms to review the effect of the variables on the target attribute 
#Split the dataset to train and test 
#Compute the logistic regression model 
#Use the test set to find the error rate 
#Submit the code and screenshots of charts 

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#Answers:

#above 50k = 1, below 50K = 0

adult <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
str(adult)

#Target = income (boolean)
#Ignoring fnlwgt (Instructions)

#Instructions
Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")

Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam")

North.america <- c("United-States" ,  "Canada", "Puerto-Rico") 

Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )

Other <- c("South")

#---------------------------------------------
#Data preperation
summary(adult) #See summary of every column in data frame

#Reduce the number of levels for country
countryFun <- function(x){
  if(x %in%  Europe) {return('Europe')}
  else if (x %in% Asia) {return("Asia")}
  else if (x %in% North.america) {return("North.america")}
  else if (x %in% Latin.and.south.america) {return("Latin.and.south.america")}
  else {return('Other')} 
}
#Use the function for change the levels
testCountry <- sapply(adult$country, countryFun)
summary(testCountry)
adult$country <- testCountry


#Reduce the number of levels for marital (3 levels) and type_employer (5 levels)

#marital -> Divorced, Married-AF-spouse, Married-civ-spouse, Married-spouse-absent, Never-married, Separated, Widowed
#3 levels -> Not Married, Married, Never Married
maritalFun <- function(x){
  x <- as.character(x) #Defined as character
  if(x=='Divorced' | x=='Widowed' | x=='Separated'){
    return('Not-married')
  }
  else if (x=='Never-married'){
    return(x)
  }
  else{
    return('Married')
  }
}
#Use the function for change the levels
testMarital <- sapply(adult$marital, maritalFun)
adult$marital <- testMarital
summary(adult)

#type_employer -> Private, Self-emp-not-inc, Local-gov, Federal-gov, State-gov, Self-emp-inc, ?, Never-worked, Without-pay
#5 levels -> Self-emp, Gov and the others
summary(adult$type_employer)
employerFun <- function(x){
  x <- as.character(x) #Defined as character
  if(x=='Federal-gov' | x=='Local-gov' | x=='State-gov'){
    return('Gov')
  }
  else if (x=='Self-emp-inc' | x=='Self-emp-not-inc'){
    return('Self-emp')
  }
  else{
    return(x)
  }
}
#Use the function for change the levels
testJob <- sapply(adult$type_employer, employerFun)
adult$type_employer <- testJob

table(adult$country) #Check

#Defined all the features we change as factor
adult$type_employer <- factor(adult$type_employer)
adult$country <- factor(adult$country)
adult$marital  <- factor(adult$marital)


#Remove all records with missing data 
#We see that some of the values are "?", that like NA (dont give us new information)
adult[adult=='?'] <- NA #Change in every cell

any(is.na(adult)) #True

#See the missing value on graph
install.packages('Amelia')
library(Amelia)

missmap(adult, col = c('yellow', 'black'))
any(is.na(adult$occupation)) #true
any(is.na(adult$type_employer)) #true

#Remove the records with NA values
adult <- na.omit(adult)
any(is.na(adult)) #False

#Remove the index column from the dataset 
install.packages('dplyr')
library(dplyr)
adult.clean <- select(adult, -X , -fnlwgt)
str(adult.clean)

#income defined as factor

#Use histograms to review the effect of the variables on the target attribute
library(ggplot2)
ggplot(adult.clean, aes(x=adult.clean$age, fill = adult.clean$income)) + geom_histogram()
#above 50K we see that most of the worker between the age of 25-50

ggplot(adult.clean, aes(x=adult.clean$hr_per_week, fill = adult.clean$income)) + geom_histogram()
#there is a peck for both of incomes in the range close to 50 hr per week

ggplot(adult.clean, aes(x=adult.clean$capital_gain, fill = adult.clean$income)) + geom_histogram()
#when the capital gain was zero were the most of the workers in both incomes

ggplot(adult.clean, aes(x=adult.clean$capital_loss, fill = adult.clean$income)) + geom_histogram()
#when the capital loss was zero were the most of the workers in both incomes

levels(adult.clean$income) #"<=50K" ">50K" 
adult.clean <- rename(adult.clean, region = country) #change from country to region
str(adult.clean)

#Split the dataset to train and test 
set.seed(101)

install.packages('caTools')
library(caTools) #sample.split function

splitDf <- sample.split(adult.clean, SplitRatio = 0.7)
train <- subset(adult.clean, splitDf ==T)
test <- subset(adult.clean, splitDf ==F)

#Compute the logistic regression model 
model <- glm(income ~ ., family = binomial(link = 'logit'), data = train)

summary(model)

?step

step.model <- step(model)

summary(step.model)

predictecProb <- predict(model, newdata = test, type = 'response')

predictedValues <- ifelse(predictecProb > 0.5, 1, 0)

#Use the test set to find the error rate 
cm <- table(test$income, predictecProb>0.5)

#        FALSE TRUE
#<=50K  7678  589
#>50K   1074 1629

#END OF EX5

