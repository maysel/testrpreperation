#Ex4 - Date Preparation and Logistic Regression [Titanic dataset]

#Look into the structure of the dataset
#Are there attributes with missing values? If there are fill the missing values with the mean value of that attribute 
#Turn Survival into a factor with the right values  
#Use ggplot histogram to investigate how Age, Fare and SibSp affect Survival (submit the charts, use print screen)
#Select relevant features for machine learning
#(We will run the ML algorithm in class)

#Answers:
#read the DB
titanic <- read.csv("titanictrain.csv", header=T)

#Look into the structure of the dataset
str(titanic)

#Are there attributes with missing values?
#If there are fill the missing values with the mean value of that attribute
any(is.na(titanic)) #true
install.packages('Amelia') #Show na values
library(Amelia)

missmap(titanic, main = "Missing data", col = c('yellow', 'black'))
#missing values only in age column
meanAge <- mean(titanic$Age[!is.na(titanic$Age)])

isNaValue <- function(x){
  if (is.na(x)){
    return(meanAge)
  } else{
    return(x)
  }
}

titanic$Age = sapply(titanic$Age, isNaValue)

any(is.na(titanic)) #False

#Turn Survival into a factor with the right values  
titanic$Survived <- factor(titanic$Survived, levels = c(0,1), labels = c('Not survived','Survived'))
str(titanic)

#Use ggplot histogram to investigate how Age, Fare and SibSp affect Survival
library(ggplot2)
ageChart <- ggplot(titanic, aes(x=Age, fill = Survived)) + geom_histogram()
#Conclusion - Between the age 20-40 most of the people didn't survived
FareChart <- ggplot(titanic, aes(x=Fare, fill = Survived)) + geom_histogram()
#Conclusion - the people who paid betwenn 0-100 the most of them didn't survived

#Select relevant features for machine learning
install.packages('dplyr')
library(dplyr)

titanic.clean <- select(titanic, -PassengerId, -Name, -Ticket, -Cabin)
str(titanic.clean)

#Look if they are values that should be factor - dummy varaible
titanic.clean$Pclass <- factor(titanic.clean$Pclass)

#---------------------------------------------
#Model
#Split to training set and test set
set.seed(101)
train <- sample_frac(titanic.clean, 0.7)
sid <- as.numeric(rownames(train))
test <- titanic.clean[-sid,]

#Run model
log.model <- glm(Survived ~ . , family = binomial(link = 'logit'), train)
summary(log.model)

str(test)
str(train)

predict.probabilities <- predict(log.model, test, type = 'response')

#defined values 1,0 - bigger than 0.5 is 1
predict.values <- ifelse(predict.probabilities > 0.5 ,1,0)

#error using mean
misClassError <- mean(predict.values != test$Survived)

#Confusion matrix
cm <- table(test$Survived,predict.probabilities>0.5)

#Results
#            FALSE TRUE
#Not survived   142   23
#Survived        34   68

#END OF EX4
