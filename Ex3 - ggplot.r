#Ex3 - ggplot
#Use the file train.csv from the bike example as the data set. 
#Generate the date and hour attributes as above (lesson #4)
#Produce the following charts and state what you learned from them. For each cart select a relevant geometry.
#Try to config each cart with color so that it will look nice.
#A chart showing how temperature affects the count
#A chart showing how date affects count 
#A chart showing how season affects count (use here a boxplot) learn here about what a boxplot is and how to interpret it
### --> (turn season into factor first, add labels to season (hint - the factor function has a label parameter)
#A chart showing how hour AND temperature together affects count. Use x axis for hour and color for temperature. Split the chart to weekdays and sunday.
###(bonus - learn how to use ggplot to show these two charts on the same page)
#A chart showing anything you like so long as it provides an interesting insight into the data

#Answers:

bike <- read.csv("train.csv", header=T)
str(bike)

#like in lesson 4 
bike$datetime <- as.character(bike$datetime) #Convert date to character
bike$date <- sapply(strsplit(bike$datetime,' '), "[", 1) #Choose only date
bike$date <- as.Date(bike$date) #Convert date to date type
bike$time <- sapply(strsplit(bike$datetime,' '), "[", 2) #Choose only time
bike$hour <-  sapply(strsplit(bike$time,':'), "[", 1) #Choose only hour
bike$hour <- as.numeric(bike$hour) #Convert hour to numeric
bike$time <- NULL #cancel column time
bike$datetime <- NULL #cancel column dateTime

install.packages('ggplot2')
library(ggplot2)

#A chart showing how temperature affects the count
#Scatter plot
tempCount <- ggplot(bike, aes(x = temp, y = count))
tempCount <- tempCount + geom_point(alpha=0.2)
#Conclusion - there is weak correlation between temperature to count

#A chart showing how date affects count
#Scatter plot
dateCount <- ggplot(bike, aes(x = date, y = count))
dateCount <- dateCount + geom_point(alpha=0.2)
#Conclusion - there is weak correlation between date to count

#A chart showing how season affects count (use here a boxplot)
#Boxplot
bike$season <- factor(bike$season , levels = c(1,2,3,4), lables<-c("spring","summer","fall","winter"))
seasonCount <- ggplot(bike, aes(x = season, y = count))
seasonCount <- seasonCount + geom_boxplot()
#Conclusion - season affects on count, between the summer and fall to spring we see big different

#A chart showing how hour AND temperature together affects count
#Split the chart to weekdays and sunday
bike$workingday <- as.factor(bike$workingday)
htCount <- ggplot(bike, aes(x = hour, y = count))
htCount <- htCount + geom_point(aes(color = bike$temp))
htCount <- htCount + facet_grid(. ~ bike$workingday)
#Conclusion - hour and temperature on working day we see that between the hour 10-20 and high temp there more counts
#Conclusion -  hour and temperature not on working day wee not see connection

#A chart showing anything you like so long as it provides an interesting insight into the data 
#count vs. registration
regiCount <- ggplot(bike, aes(x = registered, y = count))
regiCount <- regiCount + geom_point(alpha=0.2) +geom_smooth(method=lm)
#Conclusion - high correlation between them

#END OF EX3
