#The code for the Spam filter example: 
#Spam filtering Naive Base + Text mining 
#topics

# ** corpus **
# a corpus is a collections of documents containing (natural language) text
# its a data structure of documents on which the tm function work 

# ** Document Term  Matrix **
# A matrix for which the columns are words 
# and the rows are documents *a document is a collection fo words
# with some relation among them

## stop words
# very common words that do not ccarry info 

#getwd returns an absolute filepath representing the current working directory of the R process
getwd()

#stringsAsFactors = FALSE. In R strings are evalutaed as factord meaning that 
# they are actually represented as integres
# here we would like the strings to be just strings - a collections of words

spam<-read.csv('spam.csv', stringsAsFactors = FALSE)

#but.. the spam type (span or ham) is a fcator with two levels 
spam$type <- as.factor(spam$type)

#analayse the spam data frame
str(spam)

head(spam, n=10L) #Present the first records

View(spam)

#tm is a text mining package with a bunch of text mining functions 
install.packages('tm')
library(tm)

# we transform the text part into a curpus
# vector source states that the input is a vector
# the corpus optimizes the data structure for text operations
# such as some text trnasformations ans extructing a document term matrix 

spam_corpus <- Corpus(VectorSource(spam$text))


#looking at the corpus whuch is basically a list 
spam_corpus[[1]][[1]] #Value
spam_corpus[[1]][[2]] #information

# cleainin the corpus
# remove puctuation
clean_corpus <- tm_map(spam_corpus,removePunctuation)

#remove digits
clean_corpus <- tm_map(clean_corpus,removeNumbers)

#turn to lower case
clean_corpus <- tm_map(clean_corpus,content_transformer(tolower))
clean_corpus[[1]][[1]]

#remove stopwords 
clean_corpus <- tm_map(clean_corpus,removeWords, stopwords())

#  Multiple whitespace characters are collapsed to a single blank
clean_corpus <- tm_map(clean_corpus,stripWhitespace)

stopwords()

#generate the document matrix 
dtm <- DocumentTermMatrix(clean_corpus)

#inspet the dtm 
dim(dtm)

#removing infrequent terms (that do not apear at least 10 times)
frequent_dtm <-DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,10)))

#inspet the frequent_dtm
dim(frequent_dtm)
inspect(frequent_dtm[1:500,20:59])
?inspect

#genrating a word cloud of spam and not spam 
install.packages('wordcloud')
library(wordcloud)

# color pallet for the word cloud 
pal <- brewer.pal(9,'Dark2')


# general 
wordcloud(clean_corpus, min.freq = 5,
          random.order = FALSE, colors = pal)

# ham
wordcloud(clean_corpus[spam$type == 'ham'], min.freq = 5,
          random.order = FALSE, colors = pal)

#spam 
wordcloud(clean_corpus[spam$type == 'spam'], min.freq = 5,
          random.order = FALSE, colors = pal)


#Spliting to training and testing data sets
vec <- runif(500)
split <- vec > 0.3
split

#spliting the raw data
train_raw <- spam[split,]
dim(train_raw)
test_raw <- spam[!split,]
dim(test_raw)

#spliting the corpus data (Not sure we nned that)
train_corpus <- clean_corpus[split]
train_corpus
test_corpus <- clean_corpus[!split]
test_corpus

#spliting document term matrix 
train_dtm <- frequent_dtm[split,]
test_dtm <- frequent_dtm[!split,]


inspect(train_dtm[1:25,1:10])
inspect(test_dtm[1:25,1:10])

#convert the frequency matrix to yes/no  
conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x, level = c(1,0), labels = c('Yes', 'No'))
}

train <- apply(train_dtm, MARGIN = 1:2, conv_yesno)
test <- apply(test_dtm, MARGIN = 1:2, conv_yesno)
#note: not anymore document term matrix 

dim(test)
dim(train)

#convert the matrix into data frames 
df_train = as.data.frame(train)
df_test = as.data.frame(test)

#add type (ham or spam) column 
df_train$type <- train_raw$type
df_test$type <- test_raw$type

df_train[1:10,59:60]


#apllying naiive base
install.packages('e1071')
library(e1071)

#note: column 60 is the class
model <- naiveBayes(df_train[,-60],df_train$type)

model

prediction <- predict(model, df_test[,-60])
prediction


## creating confision matrix 
install.packages('SDMTools')
library(SDMTools)

conv_10 <- function(x){
  x <- ifelse( x =='spam',1,0)
}

pred01 <- sapply(prediction,conv_10)
actual01 <- sapply(df_test$type,conv_10)

confusion <- confusion.matrix(actual01, pred01)

TP <- confusion[2,2]
FP <- confusion[2,1]
TN <- confusion[1,1]
FN <- confusion[1,2]

recall <- TP/(TP+FN)
precision <- TP/(TP+FP)

#END OF CLASS 7&8